//TRY
// This program publishes randomly-generated velocity
// messages for turtlesim.
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>  // For geometry_msgs::Twist
#include <stdlib.h> // For rand() and RAND_MAX
#include <turtlesim/Pose.h>
#include <iomanip> // for std::setprecision and std::fixed

// A callback function.  Executed each time a new pose
// message arrives.



/*void poseMessageReceived(const turtlesim::Pose& pos) {
  ROS_INFO_STREAM(std::setprecision(2) << std::fixed
    << "position=(" <<  pos.x << "," << pos.y << ")"                        ///doubt - why onle msg.x can be used and not pos.x
    << " direction=" << pos.theta);
}

bool safezoneDetect(const float &c, const float &d) {
  bool a;
  if (c>3 && c<8 && d>3 && d<8)                         ///defining a square of 5x5
  {
    ROS_INFO_STREAM(std::setprecision(2) << "SAFE ZONE DETECTED");      ///doubt - std::setprecision
    a = 0;
  }
  else 
    a = 1;
  return a;
}*/


/*bool poseMessageReceived(const turtlesim::Pose& pos) {
  ROS_INFO_STREAM(std::setprecision(2) << std::fixed
    << "position=(" <<  pos.x << "," << pos.y << ")"                        ///doubt - why onle msg.x can be used and not pos.x
    << " direction=" << pos.theta);
  bool a;
  if (pos.x>3 && pos.x<8 && pos.y>3 && pos.y<8)                         ///defining a square of 5x5
  {
    ROS_INFO_STREAM(std::setprecision(2) << "SAFE ZONE DETECTED");      ///doubt - std::setprecision
    a = 0;
  }
  else 
    a = 1;
  return a;
}*/

turtlesim::Pose posit;

void poseMessageReceived(const turtlesim::Pose& pos) {
  ROS_INFO_STREAM(std::setprecision(2) << std::fixed
    << "position=(" <<  pos.x << "," << pos.y << ")"
    << " direction=" << pos.theta);
posit.x = pos.x;
posit.z = pos.y
}

int main(int argc, char **argv) {
  // Initialize the ROS system and become a node.
  ros::init(argc, argv, "publish_controlled_velocity");
  ros::NodeHandle nh;

    // Create a subscriber object.
  ros::Subscriber sub = nh.subscribe("turtle1/pose", 1000,
    &poseMessageReceived);


  // Create a publisher object.
  ros::Publisher pub = nh.advertise<geometry_msgs::Twist>(
    "turtle1/cmd_vel", 1000);

  // Seed the random number generator.
  srand(time(0));

  // Loop at 2Hz until the node is shut down.
  ros::Rate rate(2);
  while(ros::ok()) {
    // Create and fill in the message.  The other four
    // fields, which are ignored by turtlesim, default to 0.
    geometry_msgs::Twist msg;

    if (posit.x>3 && posit.x<8 && posit.y>3 && posit.y<8) 
    {
     msg.linear.x = 1;
    msg.angular.z = 2*double(rand())/double(RAND_MAX) - 1;
    }
    else

    {
      msg.linear.x = double(rand())/double(RAND_MAX);
    msg.angular.z = 2*double(rand())/double(RAND_MAX) - 1;
  }

    // Publish the message.
    pub.publish(msg);

    // Send a message to rosout with the details.
    ROS_INFO_STREAM("Sending random velocity command:"
      << " linear=" << msg.linear.x
      << " angular=" << msg.angular.z);

  // Let ROS take over.
  ros::spinOnce();
  
    // Wait until it's time for another iteration.
    rate.sleep();


  }
}
